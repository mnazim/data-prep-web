import random
from django.shortcuts import redirect, render, get_object_or_404
from django.db.models import Q
from django.http import Http404
from django.views import  generic
from harvestor.models import Tweet
from django.urls import reverse, reverse_lazy
from django.contrib.auth.decorators import login_required, permission_required
from .models import Annotation
from .forms import AnnotationForm


class AnnotatorDashboard(generic.TemplateView):
    template_name = 'triage/annotator_base.html'
annotator_dashboard = AnnotatorDashboard.as_view()

def get_random_tweet(skip_id=None):
    conds = ~Q(text__startswith='RT') & Q(annotation__isnull=True) & Q(event__annotations_enabled=True)
    if skip_id is not None:
        conds &= ~Q(id=skip_id)

    idx = random.randint(0, 99)
    return Tweet.objects.filter(conds)[idx]

@login_required()
def annotate_redirect(request, skip_id=None):
    t = get_random_tweet(skip_id)
    print('*'*88)
    print(t)
    if t is None:
        return render(request, 'triage/annotation_form.html', { 'zerotweets': True })

    return redirect(reverse('annotate', args=[t.id]))

class CreateAnnotation(generic.CreateView):
    model = Annotation
    form_class = AnnotationForm
    success_url = reverse_lazy('annotate_redirect')

    def dispatch(self, request, *args, **kwargs):
        self.tweet = self._get_tweet()
        if self.tweet is None:
            raise Http404
        r = super().dispatch(request, *args, **kwargs)
        return r

    def _get_tweet(self):
        if not hasattr(self, 'tweet'):
            self.tweet = get_object_or_404(Tweet, pk=self.kwargs['tweet_id'])
        return self.tweet

    def get_initial(self):
        return {
            'tweet': self.tweet,
            'annotator': self.request.user,
        }

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['tweet'] = self._get_tweet()
        context['has_permission'] = True
        return context


create_annotation = login_required(permission_required('triage.add_annotation')(CreateAnnotation.as_view()))
