from django.contrib import admin
from django.db import models
from django import forms
from .models import Annotation
from .forms import AnnotationForm


class AnnotationAdminForm(AnnotationForm):
    class Meta:
        fields = ('__all__')


@admin.register(Annotation)
class AnnotationAdmin(admin.ModelAdmin):
    form = AnnotationAdminForm

    list_display = ['id', 'get_tweet_text', 'aspects', 'sentiments', 'annotator']
    fields = ['get_tweet_display', 'aspects', 'sentiments', 'switches', 'annotator']
    readonly_fields = ['get_tweet_display', 'tweet_id', 'annotator']
    list_filter = ['annotator']
    list_select_related = ['tweet']
    search_fields = ['aspects', 'sentiments', 'switches', 'annotator__username']


    def get_list_filter(self, request):
        l = []
        if request.user.is_superuser:
            l.append('annotator')
        return l

    def get_queryset(self, request):
        if request.user.is_superuser:
            return Annotation.objects.all()
        return request.user.annotation_set.all()

    def get_changelist_form(self, request, **kwargs):
        print(type(self.get_changelist_instance(request)))
        kwargs.setdefault('form', AnnotationAdminForm)
        return super().get_changelist_form(request, **kwargs)

    def get_tweet_text(self, instance):
        return instance.tweet.text
    get_tweet_text.short_description = 'Text'

    def get_tweet_display(self, instance):
        return instance.tweet.get_tweet_display()
    get_tweet_display.allow_tags = True
    get_tweet_display.short_description = 'Tweet'
