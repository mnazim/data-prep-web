from django import forms
from dynamic_preferences.registries import global_preferences_registry
from .models import  Annotation

def get_aspects():
    global_preferences = global_preferences_registry.manager()
    out = [(i.strip(), i.strip().replace('_', ' ')) for i in global_preferences['triage__aspects'].split("\n") if i.strip() != '']
    return out

def get_sentiments():
    global_preferences = global_preferences_registry.manager()
    out = [(i.strip(), i.strip().replace('_', ' ')) for i in global_preferences['triage__sentiments'].split("\n") if i.strip() != '']
    return out

def get_switches():
    global_preferences = global_preferences_registry.manager()
    out = [(i.strip(), i.strip().replace('_', ' ')) for i in global_preferences['triage__switches'].split("\n") if i.strip() != '']
    return out


class AnnotationForm(forms.ModelForm):
    switches = forms.MultipleChoiceField(
        choices=get_switches(),
        widget=forms.CheckboxSelectMultiple(),
        required=False
    )

    aspects = forms.MultipleChoiceField(
        choices=get_aspects(),
        widget=forms.CheckboxSelectMultiple(),
    )

    sentiments = forms.MultipleChoiceField(
        choices=get_sentiments(),
        widget=forms.CheckboxSelectMultiple(),
    )

    class Meta:
        model = Annotation
        fields = ['annotator', 'tweet', 'aspects', 'sentiments', 'switches']
        widgets = {
            'tweet': forms.HiddenInput(),
            'annotator': forms.HiddenInput(),
        }
