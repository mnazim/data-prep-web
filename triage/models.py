from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.db import models
from dynamic_preferences.registries import global_preferences_registry


class Annotation(models.Model):
    global_preferences = global_preferences_registry.manager()
    tweet = models.ForeignKey(
        "harvestor.Tweet",
        on_delete=models.CASCADE,
    )
    aspects = ArrayField(
        models.CharField(max_length=255, blank=True),
        default=list
    )
    sentiments = ArrayField(
        models.CharField(max_length=255, blank=True),
        default=list
    )
    switches = ArrayField(
        models.CharField(max_length=255, blank=True),
        default=list
    )
    annotator = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, on_delete=models.SET_NULL)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "annotations"
