from django.urls import path
from . import views

urlpatterns = [
    path('annotation/add/', views.annotate_redirect, name='annotate_redirect'),
    path('annotation/add/<int:skip_id>/', views.annotate_redirect, name='annotate_redirect_skip'),
    path('annotate/<int:tweet_id>/', views.create_annotation, name='annotate'),
]

