
from dynamic_preferences.types import LongStringPreference
from dynamic_preferences.registries import global_preferences_registry
from dynamic_preferences.preferences import Section


triage = Section('triage')


@global_preferences_registry.register
class Aspect(LongStringPreference):
    name = 'aspects'
    section = triage
    default = "event\nticket\nlocation\ncontent\nspeaker\nannouncement\nopinion\nfood\natmosphere\nopportunity\namenities\nextra"
    help_text = 'List of aspects. One value per line.'


@global_preferences_registry.register
class Sentiment(LongStringPreference):
    name = 'sentiments'
    section = triage
    default = "neutral\nworry\ndisappointment\ncomplaint\nanger\nirritation\ncheated\nconfused\nthanks\nhappy\ndelight"
    help_text = 'List of sentiments. One value per line.'


@global_preferences_registry.register
class Switches(LongStringPreference):
    name = 'switches'
    section = triage
    default = "contains_question\ncontains_quote"
    help_text = 'List of sentiments. One value per line.'



"""
event\nticket\nlocation\ncontent\nspeaker\nannouncement\nopinion\nfood\natmosphere\nopportunity\namenities\nextra

    

event
content
speaker
announcement
opinion
ticket
food
location
atmosphere
aminities
opportunity
extra
"""