from __future__ import unicode_literals

import json
import os
from fnmatch import fnmatch
from urllib.parse import quote_plus, urlencode

import requests
import twitter
from celery import shared_task
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db.models import Max, Q
from django.utils import timezone
from requests.auth import HTTPBasicAuth

from .models import Event, Tweet


@shared_task(name="harvestor.tasks.search")
def search(phrase, event_id):
    api = twitter.Api(
        consumer_key=settings.TWITTER_API_KEY,
        consumer_secret=settings.TWITTER_API_SECRET, 
        access_token_key=settings.TWITTER_ACCESS_TOKEN, 
        access_token_secret=settings.TWITTER_ACCESS_TOKEN_SECRET,
        tweet_mode='extended',
        sleep_on_rate_limit=True
    )
    try:
        user = api.VerifyCredentials()
    except twitter.TwitterError as e:
        return str(e)

    params = {
        'q': phrase,
        'count': 10000,
        'lang': 'en',
    }
    agg = Tweet.objects.filter(event_id=event_id).aggregate(max_id=Max('id'))
    if agg['max_id'] is not None:
        params['since_id'] = agg['max_id']

    filestore = FileSystemStorage(location=settings.TWEETS_JSON_DIR)
    timestamp = timezone.now().strftime('%Y.%m.%d %H:%I:%S')
    savepath = filestore.get_available_name( os.path.join( settings.TWEETS_JSON_DIR, f'{timestamp} {phrase}.json'))

    query = urlencode(params, quote_via=quote_plus)
    try:
        res = api.GetSearch(raw_query=query)
    except twitter.TwitterError as e:
        return str(e)
    final_result = []
    for tweet in res:
        final_result.append(tweet._json)

    if len(final_result) == 0:
        return f"Twitter did give me any tweets :("

    out = {
        'event_id': event_id,
        'phrase': phrase,
        'count': len(final_result),
        'result': final_result
    }
    with open(savepath, 'w') as f:
        twitter.json.dump(out, f)

    return f"Harvested {len(res)} events; saved to {savepath}"


@shared_task(name="harvestor.tasks.harvest_event_tweets")
def harvest_event_tweets(event_id):
    try:
        e = Event.objects.get(pk=event_id)
    except Event.DoesNotExist:
        return "<Event:{event_id}> does not exist."
    tags = e.get_twitter_hashtags()
    for tag in tags:
        search.delay(tag, event_id)
    
    return f'Queued {tags}({len(tags)}) from Event:{e}'


@shared_task(name="harvestor.tasks.queue_events")
def queue_events():
    now = timezone.now()
    conditions = Q(harvest_once=True) | Q(tracking_starts__lte=now) & Q(tracking_ends__gte=now)
    events = Event.objects.filter(conditions)[:10]
    if not events.exists():
        return "No events to harvest."
    
    for e in events:
        harvest_event_tweets.delay(e.id)
        e.harvest_once = False
        e.save(update_fields=['harvest_once'])

    nl = "\n"
    return f'Queued 1 event'



@shared_task(name="harvestor.tasks.impot_tweets_file")
def import_tweets_file(file):
    bname, fname = os.path.split(file)
    if fname.startswith('IMPORTED'):
        return f"File {file} already imported"
    if not os.path.isfile(file):
        return f"File {file} does not exist."

    try:
        with open(file, 'r') as f:
            contents = json.load(f)
    except json.JSONDecodeError:
        return f"File {file} is not a valid json file."

    event_id = contents.get('event_id', None)

    total = 0
    try:
        for tweet in contents['result']:
            create_tweet(tweet, event_id)
            total += 1
    except KeyError:
        return f"No tweets found in {file}"

    fname = f'IMPORTED {fname}'
    newfile = os.path.join(bname, fname)
    os.rename(file, newfile)

    return f"Imported {total} from {file}"


@shared_task(name="harvestor.tasks.import_tweets")
def import_tweets():
    files = [f for f in os.listdir(settings.TWEETS_JSON_DIR)
             if fnmatch(f, '*.json') and not f.startswith('IMPORTED')][:25]
    for f in files:
        import_tweets_file.delay(os.path.join(settings.TWEETS_JSON_DIR, f))
    return f'Queued {len(files)} json files for importing.'


def create_tweet(tweet, event_id):
    tweet = fix_unicode_null(tweet)
    timefmt = '%a %b %d %H:%M:%S %z %Y'
    created_at = timezone.datetime.strptime(tweet['created_at'], timefmt)
    newtweet, created = Tweet.objects.get_or_create(
        id=tweet['id']
    )

    if created:
        newtweet.event_id = event_id

    newtweet.created = created_at
    newtweet.text = tweet['full_text']
    newtweet.contains_quote = tweet['is_quote_status']
    newtweet.depot['tweet'] = tweet
    newtweet.save()


def fix_unicode_null(j):
    nj = {}
    for k, v in j.items():
        if isinstance(v, dict):
            j[k] = fix_unicode_null(j[k])
        elif j[k] == '\u0000':
                j[k] = None
        elif isinstance(j[k], str) and j[k].find('\u0000') != -1:
            j[k] = j[k].replace('\u0000', '?')
    return j


def fix_dates():
    files = [f for f in os.listdir(settings.TWEETS_JSON_DIR)
             if fnmatch(f, '*.json') and f.startswith('IMPORTED')]
    total = 0
    files.reverse()
    for file in files:
        p = os.path.join(settings.TWEETS_JSON_DIR, file)
        try:
            with open(p, 'r') as f:
                contents = json.load(f)
        except json.JSONDecodeError:
            continue

        event_id = contents['event_id']
        for tweet in contents['result']:
            create_tweet(tweet, event_id)
            total += 1
            print(f'total = {total}')
