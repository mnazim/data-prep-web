import json
from django.contrib import admin
from django.db.models import Q
from django.utils.text import mark_safe
from django.utils import timezone
from django.contrib.humanize.templatetags.humanize import naturaltime
from .models import Event, Tweet


@admin.register(Event)
class EventAdmin( admin.ModelAdmin):
    list_display = ['get_name_display', 'annotations_enabled', 'tracking', 'total_tweets', 'original_tweet_count', 'annotation_count']
    fields = ['twitter_hashtags', 'tracking_starts', 'tracking_ends', 'overview', 'annotations_enabled', 'harvest_once']
    actions = ['queue_for_harvest']

    def original_tweet_count(self, instance):
        return mark_safe(f'<a href="/harvestor/tweet/?event_id__exact={instance.id}"><strong>{instance.tweets.filter(~Q(text__startswith="RT")).count()}</strong></a>')
    original_tweet_count.allow_tags = True
    original_tweet_count.short_description = 'tweets (no retweets)'

    def total_tweets(self, instance):
        return instance.tweets.count()



    def tracking(self, instance):
        if not all([instance.tracking_starts, instance.tracking_ends]):
            return 'Ambigous!'

        now = timezone.now()
        if instance.tracking_starts > now:
            return f'Starts {naturaltime(instance.tracking_starts)}'

        if instance.tracking_ends < now:
            return f'Ended {naturaltime(instance.tracking_ends)}'

        if instance.tracking_starts < now < instance.tracking_ends:
            return f'Currently tracking. Ends {naturaltime(instance.tracking_ends)}'


        fmt = '%Y/%m/%d %H:%I:%S <small>%Z</small>'
        return mark_safe(f'{instance.tracking_starts.strftime(fmt)} - {instance.tracking_ends.strftime(fmt)}')
    tracking.allow_tags = True

    def queue_for_harvest(self, request, queryset):
        row_count = queryset.update(harvest_once=True)
        if row_count == 1:
            msg = "1 event"
        else:
            msg = f'{row_count} events'
        self.message_user(request, f'{msg} queued for harvesting')
    queue_for_harvest.short_description = "Queue to harvest once"


@admin.register(Tweet)
class TweetAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'event',
        'text',
        'created',
    ]
    list_filter = ['event']
    search_fields = ['event__twitter_hashtags', 'text']
    list_select_related = ['event']
    readonly_fields = ['get_tweet_display', 'created',  'event', 'get_depot_display']
    exclude = ['depot', 'text', 'aspect', 'emotion', 'contains_quote', 'contains_question']
    list_per_page = 10

    def get_queryset(self, request):
        return Tweet.objects.filter(~Q(text__startswith='RT'))

    def has_add_permission(self, *a, **kw):
        return False

    def has_delete_permission(self, *a, **kw):
        return False

    def get_depot_display(self, instance):
        return mark_safe(f'<pre><code style="display:block; height: 300px;  width: 500px; overflow: auto; font-weight: bold">'
                         f'{json.dumps(instance.depot, indent=2)}</code></pre>')
    get_depot_display.allow_tags = True
    get_depot_display.short_description = 'Depot'


