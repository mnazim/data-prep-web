from django.apps import AppConfig


class HarvestorConfig(AppConfig):
    name = 'harvestor'
