from django.db import models
from django.db.models import Count
from django.utils.text import mark_safe
from model_utils.choices import Choices
from model_utils.fields import StatusField
from django.contrib.postgres.fields import JSONField


class Event(models.Model):
    twitter_hashtags = models.TextField(
        help_text='A list of hashtags and search phrases. One per line. '
    )
    name = models.CharField(
        max_length=256,
        null=True, blank=True,
    )
    tracking_starts = models.DateTimeField(null=True)
    tracking_ends = models.DateTimeField(null=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    harvest_once = models.BooleanField(
        default=True,
        help_text="Fetch tweets (max 100) for this events once. One time; auto-resets after fetch."
    )
    annotations_enabled = models.BooleanField(
        default=False,
        help_text="Make the tweets under this event available to annotators."
    )

    overview = models.TextField(
        null=True, blank=True,
        help_text="Will be displayed to the annotators in case the need to understand what event is about."
    )

    class Meta:
        db_table = 'tags'

    def __str__(self):
        return self.get_name_display();

    def get_name_display(self):
        if self.name is None:
            return f'{self.twitter_hashtags}'
        return f'{self.twitter_hashtags} ({self.name})'

    def get_twitter_hashtags(self):
        out = []
        for tag in self.twitter_hashtags.split("\n"):
            tag = tag.strip()
            if len(tag) > 1:
                out.append(tag)
        return out

    def annotation_count(self):
        r = self.tweets.prefetch_related('annotation_set').filter(annotation__isnull=False).aggregate(num=Count('annotation'))
        return r['num']


class Tweet(models.Model):
    EMOTIONS = Choices(
        'neutral',
        'worry',
        'disappointment',
        'complaint',
        'anger',
        'irritation',
        'cheated',
        'confused',
        'thanks',
        'happy',
        'delight',
    )

    ASPECTS = Choices(
        'event',
        'ticket',
        'food',
        'location',
        'atmosphere',
        'aminities',
        'opportunity',
        'content',
        'announcement',
        'opinion',
        'extras',
    )

    id = models.BigIntegerField(primary_key=True, editable=False)
    text = models.TextField()
    contains_quote = models.BooleanField(default=False)
    contains_question = models.BooleanField(default=False)
    created = models.DateTimeField(auto_now_add=True)
    event = models.ForeignKey(
        Event,
        related_name='tweets',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    aspect = StatusField(choices_name='ASPECTS', null=True, blank=True, default=None)
    emotion = StatusField(choices_name='EMOTIONS', null=True, blank=True, default=None)
    depot = JSONField(default=dict)

    class Meta:
        db_table = 'tweets'

    def __str__(self):
        return f'{self.id}'

    def get_tweet_display(self):
        return mark_safe(f'<p class="twitter-tweet" data-id="{self.id}">{self.text}</p>')
    get_tweet_display.allow_tags = True
    get_tweet_display.short_description = 'Tweet'

