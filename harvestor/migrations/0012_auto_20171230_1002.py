# Generated by Django 2.0 on 2017-12-30 10:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('harvestor', '0011_event_annotations_enabled'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='annotations_enabled',
            field=models.BooleanField(default=False, help_text='Make the tweets under this event available to annotators.'),
        ),
        migrations.AlterField(
            model_name='event',
            name='created',
            field=models.DateTimeField(auto_now_add=True),
        ),
        migrations.AlterField(
            model_name='event',
            name='harvest_once',
            field=models.BooleanField(default=True, help_text='Fetch tweets (max 100) for this events once. One time; auto-resets after fetch.'),
        ),
        migrations.AlterField(
            model_name='event',
            name='modified',
            field=models.DateTimeField(auto_now=True),
        ),
    ]
