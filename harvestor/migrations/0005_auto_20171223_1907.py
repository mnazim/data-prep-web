# Generated by Django 2.0 on 2017-12-23 19:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('harvestor', '0004_auto_20171223_1034'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='tracking_ends',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='event',
            name='tracking_starts',
            field=models.DateTimeField(null=True),
        ),
    ]
