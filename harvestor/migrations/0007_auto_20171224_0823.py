# Generated by Django 2.0 on 2017-12-24 08:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('harvestor', '0006_auto_20171224_0808'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='contains_question',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='tweet',
            name='contains_quote',
            field=models.BooleanField(default=False),
        ),
    ]
