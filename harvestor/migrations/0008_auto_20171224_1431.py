# Generated by Django 2.0 on 2017-12-24 14:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('harvestor', '0007_auto_20171224_0823'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tweet',
            name='id',
            field=models.BigIntegerField(primary_key=True, serialize=False),
        ),
    ]
