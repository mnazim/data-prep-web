import os
from datetime import datetime
from fabric.api import *

env.hosts = ['tea.mnazim.co']
env.user = 'ubuntu'
env.key_filename = '/home/nazim/.ssh/id_rsa'
env.tweetsbackup_dir = '/home/nazim/labs/tea-backups/tweets.json/'
env.dbbackup_dir = '/home/nazim/labs/tea-backups/'


def backup_database():
    timefmt = '%Y%m%d%H%I%S'
    filename = f'DataPrep{datetime.utcnow().strftime(timefmt)}.pgdump'
    dumppath = os.path.join('/sites/', filename)
    dumpcmd = f'PGPASSWORD=P@ssw0rd! pg_dump -Fc --no-acl --no-owner -h localhost -U developer -d dataprep -f {dumppath}'
    run(dumpcmd)
    return dumppath


def download_database():
    dumpPath = backup_database()
    dlcmd = f'scp {env.user}@{env.host_string}:{dumpPath} {env.dbbackup_dir}'
    local(dlcmd)


def download_raw_tweets():
    cmd = f'rsync -chavzP --stats ubuntu@tea.mnazim.co:/sites/tweets.json/* {env.tweetsbackup_dir}'
    local(cmd)


def backup():
    download_database()
    download_raw_tweets()


