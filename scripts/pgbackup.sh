#!/usr/bin/env bash

if [ $# -lt 3 ]
then
    echo "Error: No arguments specified!"
    echo "Usage: pgbackup.sh DB_NAME DB_USER DB_PASSWORD S3_BUCKET_NAME"
    exit
fi


DB_NAME=$1
DB_USER=$2
DB_PASS=$3

TIMESTAMP=$(date +%F_%T | tr ':' '-')
TEMP_FILE=$(mktemp /tmp/tmp.XXXXXXXXXX)

echo 'PGPASSWORD=${DB_PASS} pg_dump -Fc --no-acl --no-owner  -U $DB_USER -d $DB_NAME -f $TEMP_FILE'
